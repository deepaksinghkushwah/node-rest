const dotenv = require("dotenv");
const express = require("express");
const morgan = require("morgan");
const color = require("colors");
const connectDB = require("./config/db");
const errorHandler= require("./middlewares/error");
const fileupload=  require('express-fileupload');
const cookieParser = require("cookie-parser");


const path = require("path");
dotenv.config({ path: "./config/config.env" });


const db =  require("./config/db");
const app = express();

// body parser
app.use(express.json())

const port = process.env.PORT || 8000;
connectDB();
// dev logger middleware
if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
}

// set file upload
app.use(fileupload());

// set static folder
app.use(express.static(path.join(__dirname, 'public')));

// cookie
app.use(cookieParser());

// route files
const bookRoutes = require("./routes/books");
const courseRoutes = require("./routes/courses");
const authRoutes = require("./routes/auth");

const { required } = require("nodemon/lib/config");
const fileUpload = require("express-fileupload");
const { cookie } = require("express/lib/response");

// roues setup
app.use("/api/v1/books", bookRoutes);
app.use("/api/v1/courses", courseRoutes);
app.use("/api/v1/auth", authRoutes);
// mount error handler
app.use(errorHandler);

const server = app.listen(port, () => {
  console.log(`Example app running on ${port}`.yellow.bold);
});

// handle unhandle rejections
process.on('unhandledRejection', (err, promise)=>{
    console.log(`Errpr: ${err.message}`.red);
    server.close(()=>process.exit(1));
});
