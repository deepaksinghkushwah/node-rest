const fs = require("fs");
const mongoose = require("mongoose");
const colors = require("colors");
const dotenv = require("dotenv");
const { faker } = require("@faker-js/faker");
//Load env vars
dotenv.config({ path: "./.env" });

// load models
const book = require("./models/Books");
const course = require("./models/Courses");

// connect database
mongoose.connect(process.env.DNS);

// read json files
const books = JSON.parse(
  fs.readFileSync(`${__dirname}/_data/books.json`, "utf-8")
);

// import into db
const importData = async () => {
  /*try {
    await book.create(books);
    console.log("Data imported".green.inverse);
    process.exit();
  } catch (error) {
    console.log(error.message);
  }*/
  try {
    const ids = [];
    for (i = 1; i < 100; i++) {
      const obj = {
        name: faker.name.findName(),
        description: faker.commerce.productDescription(),
        price: faker.commerce.price(),
      };
      let b = await book.create(obj);
      ids.push(b._id);
      //console.log(b._id);
    }
    
    
    for (i = 1; i < 500; i++) {
      const randBook = book.aggregate()._id;
      const courseObj = {
        title: faker.name.findName(),
        description: faker.commerce.productDescription(),
        weeks: faker.datatype.number({ min: 1, max: 10 }),
        tution: faker.commerce.price(),
        minimumSkill: faker.helpers.randomize([
          "beginner",
          "intermediate",
          "advanced",
        ]),
        book: ids[Math.floor(Math.random() * ids.length)]
      };

      await course.create(courseObj);
    }

    console.log("Data imported".green.inverse);
    process.exit();
  } catch (error) {
    console.log(error);
  }
};

const deleteData = async () => {
  try {
    await book.deleteMany();
    console.log("Data destroyed".red.inverse);
    process.exit();
  } catch (error) {
    console.log(error.message);
  }
};

if (process.argv[2] === "-i") {
  importData();
} else if (process.argv[2] == "-d") {
  deleteData();
}
