const express = require("express");
const router = express.Router();
const { protect, authorize } = require("../middlewares/auth");
const {
  getCourses,
  getCourseByID,
  createCourse,
  deleteCourse,
  updateCourse,
} = require("../controllers/courses");
const Course = require("../models/Course");
const advancedResults = require("../middlewares/advancedResults");
router
  .route("/")
  .get(
    advancedResults(Course, {
      path: "book",
      select: "name description",
    }),
    getCourses
  )
  .post(protect,authorize('publisher','admin'), createCourse);

router.route("/:id").get(getCourseByID).put(protect,authorize('publisher','admin'), updateCourse).delete(protect,authorize('publisher','admin'), deleteCourse);

module.exports = router;
