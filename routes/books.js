const express = require("express");
const router = express.Router();
const { protect, authorize } = require("../middlewares/auth");
const {
  getBooks,
  getBookByID,
  createBook,
  deleteBook,
  updateBook,
  bookPhotoUpload,
} = require("../controllers/books");
const Book = require("../models/Book");
const advancedResults = require("../middlewares/advancedResults");

router
  .route("/")
  .get(advancedResults(Book, "courses"), getBooks)
  .post(protect,authorize('publisher','admin'), createBook);

router.route("/:id").get(getBookByID).put(protect,authorize('publisher','admin'), updateBook).delete(protect,authorize('publisher','admin'), deleteBook);

router.route("/:id/photo").put(protect,authorize('publisher','admin'), bookPhotoUpload);

module.exports = router;
