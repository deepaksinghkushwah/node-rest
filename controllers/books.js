const Book = require("../models/Book");
const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middlewares/async");
const path = require('path');
/**
 * @desc Get all books
 * @route GET /api/vi/books
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
exports.getBooks = asyncHandler(async (req, res, next) => {  
  res.status(200).json(res.advancedResults);
});

/**
 * @desc Get book by id
 * @route GET /api/vi/books/:id
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
exports.getBookByID = asyncHandler(async (req, res, next) => {
  const id = req.params.id;
  const books = await Book.findById(id);
  if (!books) {
    return next(new ErrorResponse("Book not found", 404));
  }
  res.status(200).json({
    success: true,
    data: books,
  });
});

/**
 * @desc Create book
 * @route POST /api/v1/books
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
exports.createBook = asyncHandler(async (req, res, next) => {
  // add user to body
  req.body.user =  req.user.id;
  // check for published books
  const publishedBooks = await Book.findOne({user: req.user.id});
  // if user is not admin, they can only add one book
  if(publishedBooks && req.user.role != 'admin'){
    return next(new ErrorResponse(`User with id ${req.user.id} already published a book`, 400));  
  }

  const book = await Book.create(req.body);
  res.status(201).json({
    data: book,
    success: true,
  });
});

/**
 * @desc Update book
 * @route POST /api/v1/books/:id
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
exports.updateBook = asyncHandler(async (req, res, next) => {
  let id = req.params.id;
  const book = await Book.findOneAndUpdate(id, req.body, {
    new: true,
    runValidators: true,
  });

  if (!book) {
    return next(new ErrorResponse("Book not found", 404));
  }
  res.status(200).json({
    success: true,
    data: book,
  });
});

/**
 * @desc Create book
 * @route DELETE /api/v1/books/:id
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
exports.deleteBook = asyncHandler(async (req, res, next) => {
  const id = req.params.id;
  const book = await Book.findById(id);
  if (!book) {
    return next(
      new ErrorResponse(`Book not found with id of ${req.params.id}`, 404)
    );
  }
  book.remove();
  res.status(200).json({
    success: true,
    data: {},
  });
});


/**
 * @desc Upload photo
 * @route PUT /api/v1/books/:id/photo
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
 exports.bookPhotoUpload = asyncHandler(async (req, res, next) => {
  const id = req.params.id;
  const book = await Book.findById(id);
  
  if (!book) {
    return next(
      new ErrorResponse(`Book not found with id of ${req.params.id}`, 404)
    );
  }
  if(!req.files){
    return next(new ErrorResponse(`Please upload a file`, 400));
  }

  const file = req.files.file;
  // make sure image is a photo
  if(!file.mimetype.startsWith('image')){
    return next(new ErrorResponse(`Please upload an image file`, 400));
  }
  // check file size
  if(file.size > process.env.MAX_FILE_UPLOAD){
    return next(new ErrorResponse(`File cannot be exceed more than ${process.env.MAX_FILE_UPLOAD} bytes`, 400));
  }

  // create custom filename
  file.name =`photo_${book._id}${path.parse(file.name).ext}`;
  file.mv(`${process.env.FILE_UPLOAD_PATH}/${file.name}`, async err => {
    if(err){
      console.error(err);
      return next(new ErrorResponse(`Problem with file upload`, 500));
    }

    await Book.findByIdAndUpdate(req.params.id, {
      photo: file.name
    });

    res.status(200).json({
      success: true,
      data: file.name
    });

  });
  
});