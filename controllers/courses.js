const Course = require("../models/Course");
const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middlewares/async");

/**
 * @desc Get all courses
 * @route GET /api/vi/courses
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
exports.getCourses = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

/**
 * @desc Get course by id
 * @route GET /api/vi/course/:id
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
exports.getCourseByID = asyncHandler(async (req, res, next) => {
  const id = req.params.id;
  const items = await Course.findById(id);
  if (!items) {
    return next(new ErrorResponse("Course not found", 404));
  }
  res.status(200).json({
    success: true,
    data: items,
  });
});

/**
 * @desc Create course
 * @route POST /api/v1/course
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
exports.createCourse = asyncHandler(async (req, res, next) => {
  const item = await Course.create(req.body);
  res.status(201).json({
    data: item,
    success: true,
  });
});

/**
 * @desc Update course
 * @route POST /api/v1/course/:id
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
exports.updateCourse = asyncHandler(async (req, res, next) => {
  let id = req.params.id;
  const item = await Course.findOneAndUpdate(id, req.body, {
    new: true,
    runValidators: true,
  });

  if (!item) {
    return next(new ErrorResponse("Course not found", 404));
  }
  res.status(200).json({
    success: true,
    data: item,
  });
});

/**
 * @desc delete course
 * @route DELETE /api/v1/books/:id
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
exports.deleteCourse = asyncHandler(async (req, res, next) => {
  const id = req.params.id;
  const item = await Course.findByIdAndDelete(id);
  if (!item) {
    return next(
      new ErrorResponse(`Course not found with id of ${req.params.id}`, 404)
    );
  }
  res.status(200).json({
    success: true,
    data: {},
  });
});
