const mongoose = require("mongoose");
const CourseSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        unique: true,
        trim: true,
        maxlength: [255, 'Name can not be more than 255 chars']
    },
    description: {
        type: String,
        required: [true, 'Description required'],
        trim: true,
        maxlength: [255, 'Description can not be more than 255 chars']
    },    
    weeks: {
        type: String,
        required: [true, 'Please add number of week']
    },
    tution: {
        type: Number,
        required: [true, 'Please add tution cost']
    },
    minimumSkill: {
        type: String,
        enum: ['beginner', 'intermediate', 'advanced'],
        required: [true, 'Please add minimum skill']
    },
    scholarshipAvailable: {
        type: Boolean,
        default: false,        
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    book: {
        type: mongoose.Schema.ObjectId,
        ref: 'Book',
        required: true
    }
});

// static method of get avg course tutions
CourseSchema.statics.getAverageCost = async function(bookId){
    console.log("Calculating avg cost... ".blue);

    const obj = await this.aggregate([
        {
            $match: {book: bookId}
        },
        {
            $group: {
                _id: '$book',
                averageCost: { $avg: `$tution`}
            }
        }
    ]);
    //console.log(obj);
    try {
        await this.model('Book').findByIdAndUpdate(bookId, {
            averageCost: Math.ceil(obj[0].averageCost / 10) * 10
        })
    } catch(err){
        console.error(err);
    }
}


// get average const after save
CourseSchema.post('save',function(){
    this.constructor.getAverageCost(this.book);
});

// get average const before remove
CourseSchema.pre('remove',function(){
    this.constructor.getAverageCost(this.book);
});

module.exports = mongoose.model('Course', CourseSchema);