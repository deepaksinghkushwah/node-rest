const mongoose = require("mongoose");
const slugify = require("slugify");
const BookSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        unique: true,
        trim: true,
        maxlength: [255, 'Name can not be more than 255 chars']
    },
    slug: {
        type: String,
        slug: "name", 
        unique: true,       
    },
    description: {
        type: String,
        required: [true, 'Description required'],
        trim: true,
        maxlength: [255, 'Description can not be more than 255 chars']
    },
    price: Number,
    photo: {
        type: String,
        default: 'noimg.jpg'
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    user: {
        type: mongoose.Schema.ObjectId,
        ref: 'User',
        required: true
    }
},{
    toJSON: {virtuals: true},
    toObject: {virtuals: true}
});

// create slug from name
BookSchema.pre("save", function(next) {
    this.slug = slugify(this.name,{lower: true});    
    next();
});

// cascade delete courses when book deleted
BookSchema.pre('remove', async function(next){
   await this.model('Course') .deleteMany({
       book: this._id
   });
   next();
});

// create virtual || reverse populate
BookSchema.virtual('courses', {
    ref: 'Course',
    localField: '_id',
    foreignField: 'book',
    justOne: false
});


module.exports = mongoose.model('Book', BookSchema);